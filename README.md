# Stopwatch Application

A simple and elegant stopwatch application built using vanilla JavaScript. This project provides a user-friendly interface to start, stop, and reset the timer.

## Features

### Start Timer
- **Start Button**: On click of the start button, the timer starts.
- If the timer is at 0, it starts from the beginning; otherwise, it starts from wherever it last stopped.

### Stop Timer
- **Stop Button**: Stops the timer once clicked.

### Reset Timer
- **Reset Button**: On click of reset, the watch stops (if already started) and sets the timer to 0.

## Technologies Employed

- Vanilla JavaScript (No libraries or frameworks allowed)
- CSS (Any CSS framework like Bootstrap can be used)

## Requirements

A modern web browser that supports HTML5, CSS3, and JavaScript.

## Installation and Running

1. Clone the GitHub repository:
   ```bash
   $ git clone <repoLink>

2. Open index.html in a browser


## Directory Structure
```
[STOPWATCH]
│ index.html
│ README.md
│
├───css
│ style.css
│
└───js
main.js
```

## Support
Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.